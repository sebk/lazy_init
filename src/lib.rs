//! like `lazy_static!`, but generic.
use std::any::TypeId;
use std::collections::HashMap;
use std::sync::{RwLock, Once};
use std::ptr;

static GLOBAL_INIT: Once = Once::new();
static mut GLOBAL_MAP: *mut GenericMap = ptr::null_mut();

fn get_global_map() -> &'static GenericMap {
    unsafe {
        GLOBAL_INIT.call_once(|| {
            GLOBAL_MAP = Box::into_raw(Box::new(GenericMap::new()));
        });
        
        &*GLOBAL_MAP
    }
}

/// The underlying map.
pub struct GenericMap {
    // we use usize to store an opaque pointer
    // we can return &'static references, because no item is ever deallocated
    map: RwLock<HashMap<TypeId, usize>>
}
impl GenericMap {
    pub fn new() -> GenericMap {
        GenericMap {
            map: RwLock::new(HashMap::new())
        }
    }
    /* get an entry for `T` if there is one */
    pub fn get<T: Sync + 'static>(&self) -> Option<&T> {
        let id = TypeId::of::<T>();
        let lock = self.map.read().unwrap();
        // if an entry was found, cast it to &T
        lock.get(&id).map(|&ptr| unsafe { &*(ptr as *const T) })
    }
    // get an entry for `T` if there is one, or create one with `init`.
    // if there is a race condition, the first 
    pub fn get_or_init<T: Sync + 'static>(&self, init: impl FnOnce() -> T) -> &T {
        let id = TypeId::of::<T>();
        self.get().unwrap_or_else(|| {
            use std::collections::hash_map::Entry;
            // run init before locking again
            let new_entry = Box::new(init());
            
            // get a write lock
            let mut lock = self.map.write().unwrap();
            
            // now convert the box into the opaque pointer
            let ptr = match lock.entry(id) {
                Entry::Vacant(e) => *e.insert(Box::into_raw(new_entry) as usize),
                Entry::Occupied(e) => *e.get()
            };
            
            // return the entry that is now in the map
            unsafe { &*(ptr as *const T) }
        })
    }
    /* get the default entry for `T` (same as `get_or_init(T::default())`) */
    pub fn get_or_default<T: Sync + Default + 'static>(&self) -> &T {
        self.get_or_init(T::default)
    }
}

/* for each `T`, `init` is mostly run only once and the result of the first call is returned each time */
pub fn lazy_init<T: Sync + 'static>(init: impl FnOnce() -> T) -> &'static T {
    get_global_map().get_or_init(init)
}
